# Basic NodeJS backend with express, graphql (apollo server v2.0), webpack, swagger and babel
# EN description (CZ is below)

## Install
```bash
npm install
```

## Run
```bash
npm start
```

## Url enpoinds
- REST status endpint [http://localhost:4998/api/v1/global/endpoints](http://localhost:4998/api/v1/global/endpoints)
list of endpoints

## Swagger documentations
[http://localhost:4998/doc/api-docs](http://localhost:4998/doc/api-docs)

## Configuration
Project uses **dotenv** npm package. If you want to use this library please add .env file to root of project.

### Example
.env file
```
PORT=4998
```
then **process.env.PORT** has 4998 value

# CZ popis

## Instalace
```bash
npm install
```

## Spusteni
```bash
npm start
```

## Url adresy
- REST endpoint adresa [http://localhost:4998/api/v1/global/endpoints](http://localhost:4998/api/v1/global/endpoints)
Na teto adrese se zobrazi vsechny, ktere jsou dostupne

## Swagger dokumentace
[http://localhost:4998/doc/api-docs](http://localhost:4998/doc/api-docs)

## Konfigurace
Projekt pouziva **dotenv** npm package (balicek). Jestli toto chcete pouzit tuto knihovnu tak prosim pridejte **.env** soubor do root adresare aplikace.

### Priklad
.env soubor
```
PORT=4998
```
potom **process.env.PORT** ma hodnotu 4998