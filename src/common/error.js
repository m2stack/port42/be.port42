import Logger from '../utils/logger';

export const errorHandler = (err, req, res, next) => {
    Logger.error(`Error ${err}`);
    Logger.error(`Error while handling ${req.method} ${req.path}`);
    if (err.status === 401) {
        Logger.error('- auth failed: should log as an incident?', err);
        res.status(401).send({ message: 'Invalid token' });
    } else {
        Logger.error(`- unhandled error type: code = ${err.code} - returning status 500`);
        Logger.error(`- reason: ${err.message}`);
        next(err);
    }
};

export default errorHandler;
