/* eslint-disable no-unused-vars */
class NotAuthorize extends Error {
    constructor(message) {
        super('not.authorized', message);
    }
}
