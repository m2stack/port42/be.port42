import config from '../config';
import schema from './schema';

export default (app, path) => {
    schema.applyMiddleware({
        app, path: !path ? config.graphqlPath : path,
    });
};
