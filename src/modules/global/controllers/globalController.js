import configFile from '../../../config';

const VERSION = require('../../../../package.json').version;

export default config => ({
    status: async () => Promise.resolve().then(() => ({
        allSystemWorking: true,
        serverName: config.server.serverName,
        version: `${VERSION}-be.port42`,
    })),
    config: async () => Promise.resolve().then(() => ({
        env: config.server.env,
        port: config.server.port,
        shutdownPort: config.server.shutdownPort,
        shutdownTimeout: config.server.shutdownTimeout,
        serverName: config.server.serverName,
        dontUseGraphql: config.server.dontUseGraphql,
        corsDomain: config.server.corsDomain,
        graphqlPath: config.server.graphqlPath,
    })),
    routes: async () => {
        const endpoints = configFile.routes;
        endpoints.push({
            path: `/doc${config.server.swaggerPath}`,
        });
        if (!config.server.dontUseGraphql) {
            endpoints.push({
                path: config.server.graphqlPath,
            });
        }
        return Promise.resolve().then(() => ({
            endpoints,
        }));
    },
});
