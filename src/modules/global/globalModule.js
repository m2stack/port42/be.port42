import { Router } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { errorHandler } from '../../common/error';
import { setupModule } from '../../utils/registrator';
import GlobalController from './controllers/globalController';
import { create } from './common/globalContext';

const allAuthenticated = () => true;

export default (config) => {
    const globalController = GlobalController(config);
    const pathMapping = {
        '/status': {
            get: {
                action: globalController.status,
                authorization: allAuthenticated,
            },
        },
        '/config': {
            get: {
                action: globalController.config,
                authorization: allAuthenticated,
            },
        },
        '/endpoints': {
            get: {
                action: globalController.routes,
                authorization: allAuthenticated,
            },
        },
    };
    const router = Router();
    router.use(bodyParser.json());
    router.use(cors());
    setupModule(pathMapping, router, create);
    router.use(errorHandler);
    return router;
};
