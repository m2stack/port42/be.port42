import express, { Router } from 'express';
import path from 'path';
import { errorHandler } from '../../common/error';

export default () => {
    const router = Router();

    const DIST_DIR = __dirname;
    const HTML_FILE = path.join(DIST_DIR, 'index.html');
    router.use(express.static(DIST_DIR));
    router.get('', (req, res) => {
        res.sendFile(HTML_FILE);
    });

    router.use(errorHandler);
    return router;
};
