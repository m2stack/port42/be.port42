import { Router } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import { errorHandler } from '../../common/error';
import SwaggerController from './controllers/swaggerController';

export default (config, service) => {
    const swaggerController = SwaggerController(config, service);
    const swaggerDocument = swaggerController.getSwaggerDocument();
    const router = Router();
    router.use(bodyParser.json());
    router.use(cors());
    router.use(config.server.swaggerPath, swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    router.use(errorHandler);
    return router;
};
