export default class GlobalContext {
    constructor(loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}

export const create = req => new GlobalContext(req.user);
