import configFile from '../../../config';

const VERSION = require('../../../../package.json').version;

export default config => ({
    status: async (ctx, req, res) => Promise.resolve().then(() => {
        const user = ctx.loggedInUser;
        user.version = VERSION;
        if (user.user === 'martin') {
            return ({ status: 'ok martin' });
        }
        if (user.user === 'pavel') {
            res.status(403);
            return ({ newToken: '123' });
        }

        return res.status(200).send({ status: 'yes' });
    }),
    // config: async () => Promise.resolve().then(() => ({
    //     env: config.server.env,
    //     port: config.server.port,
    //     shutdownPort: config.server.shutdownPort,
    //     shutdownTimeout: config.server.shutdownTimeout,
    //     serverName: config.server.serverName,
    //     dontUseGraphql: config.server.dontUseGraphql,
    //     corsDomain: config.server.corsDomain,
    //     graphqlPath: config.server.graphqlPath,
    // })),
    config: async (ctx, req, res) => Promise.resolve().then(() => {
        res.status(403);
        return ({
            status: 'asd',
        });
    }),
    routes: async () => {
        const endpoints = configFile.routes;
        endpoints.push({
            path: `/doc${config.server.swaggerPath}`,
        });
        if (!config.server.dontUseGraphql) {
            endpoints.push({
                path: config.server.graphqlPath,
            });
        }
        return Promise.resolve().then(() => ({
            endpoints,
        }));
    },

    testPost: async (ctx, req, res) => Promise.resolve().then(() => {
        const user = ctx.loggedInUser;
        user.version = VERSION;
        if (user.user === 'martin') {
            return res.status(200).send({ status: 'ok martin', body: req.body });
        }
        if (user.user === 'pavel') {
            return res.status(403).send({
                newToken: '123',
                body: req.body,
            });
        }

        return res.status(200).send({ status: 'yes' });
    }),

    refreshToken: async (ctx, req, res) => Promise.resolve().then(() => {
        const user = ctx.loggedInUser;
        if (user.user === 'pavel') {
            return ({
                newToken: '123',
                status: 'refreshToken',
                body: req.body,
            });
        }
        return ({
            status: 'token is ok',
        });
    }),
});
