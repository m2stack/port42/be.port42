import { Router } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import { Strategy as BearerTokenStrategy } from 'passport-http-bearer';
import { errorHandler } from '../../common/error';
import { setupModule } from '../../utils/registrator';
import GlobalController from './controllers/globalController';
import { create } from './common/globalContext';

const allAuthenticated = () => true;

export default (config) => {
    const globalController = GlobalController(config);
    const pathMapping = {
        '/status': {
            get: {
                action: globalController.status,
                authorization: allAuthenticated,
            },
        },
        '/testpost': {
            post: {
                action: globalController.testPost,
                authorization: allAuthenticated,
            },
        },
        '/config': {
            get: {
                action: globalController.config,
                authorization: allAuthenticated,
            },
        },
        '/endpoints': {
            get: {
                action: globalController.routes,
                authorization: allAuthenticated,
            },
        },
        '/refreshtoken': {
            post: {
                action: globalController.refreshToken,
                authorization: allAuthenticated,
            },
        },
    };
    const router = Router();
    const corsOptions = {
        origin: '*',
    };
    router.use(cors(corsOptions));

    passport.use('be-port42-auth', new BearerTokenStrategy((token, callback) => {
        if (!token || token === '') {
            return callback('No auth');
        }

        if (token === '123') {
            return callback(null, { user: 'martin' });
        }

        if (token === '321') {
            return callback(null, { user: 'pavel' });
        }


        return callback(null, false);
    }));

    router.use(passport.authenticate('be-port42-auth', { session: false, failWithError: true }));

    router.use(bodyParser.json());

    setupModule(pathMapping, router, create);
    router.use(errorHandler);
    return router;
};
