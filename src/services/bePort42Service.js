import SwaggerService from './swagger/swaggerService';

export default class BePort42Service {
    constructor(config) {
        this.config = config;
        this.swaggerService = new SwaggerService(config);
    }
}
