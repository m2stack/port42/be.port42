const constants = {
    graphqlDefaultPath: '/api/v1/graphql',
    swaggerDefaultPath: '/api-docs',
    apiDefaultBasePath: '/api/v1',
    httpDefaultSchema: 'http://',
};

export default constants;
