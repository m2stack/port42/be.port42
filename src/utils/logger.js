import * as cluster from 'cluster';
import * as mkdirp from 'mkdirp';
import * as path from 'path';
import { transports, format, createLogger } from 'winston';

const {
    combine, timestamp, label, printf, colorize,
} = format;

const myFormat = printf(info => `${info.level}: ${info.message}`);

const fileFormat = printf(info => `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`);

const loggingConfig = {
    file: {
        level: 'error',
        filename: 'beport42.log',
        handleExceptions: true,
        format: combine(
            colorize(),
            label({ label: 'be.port42' }),
            timestamp(),
            fileFormat,
        ),
        json: true,
        maxsize: 5242880,
        maxFiles: 100,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        format: combine(
            colorize(),
            label({ label: 'be.port42' }),
            timestamp(),
            myFormat,
        ),
    },
    directory: path.join(__dirname, '../../logs'),
};

loggingConfig.file.filename = `${loggingConfig.directory}/${loggingConfig.file.filename}`;

if (cluster.isMaster) {
    mkdirp.sync(loggingConfig.directory);
}

export const logger = createLogger({
    transports: [
        new transports.File(loggingConfig.file),
        new transports.Console(loggingConfig.console),
    ],
    exitOnError: false,
});

export const skip = (req, res) => res.statusCode >= 200;

export const stream = {
    write: (message) => {
        logger.info(message);
    },
};

export default logger;
