import Logger from './logger';
import loadModule from './loader';
import NotAuthorized from '../common/errors/notAuthorized';

const wrap = (handler, contextCreator) => (
    req,
    res,
    next,
) => {
    const context = contextCreator(req);
    return Promise.resolve(handler.authorization(context))
        .then(isAuthorized => (!isAuthorized
            ? Promise.resolve(new NotAuthorized('Access only for authorized users'))
            : handler.action(context, req, res)))
        .then((resultOrError) => {
            if (resultOrError instanceof Error) {
                Logger.error(`error while handling request ${req.path}: ${JSON.stringify(resultOrError)}`);
                return next(resultOrError);
            }
            return res.send(resultOrError);
        })
        .catch(next);
};

export const setupModule = (moduleDef, router, contextCreator) => {
    const registerRouter = (path) => {
        const endpointDef = moduleDef[path];
        if (typeof endpointDef === 'function') {
            router.get(path, wrap(endpointDef, contextCreator));
        } else {
            Object.keys(endpointDef)
                .forEach(method => router[method](path, wrap(endpointDef[method], contextCreator)));
        }
    };
    Object.keys(moduleDef).forEach(registerRouter);
};

export default (app, modules, service, config) => {
    Object.keys(modules).forEach((pathPrefix) => {
        const module = modules[pathPrefix];
        Logger.debug(`registering module on '${pathPrefix}'`);
        app.use(pathPrefix, loadModule(module, service, config));
    });
};
